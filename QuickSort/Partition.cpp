#include "ServiceStuff.h"

unsigned char GetPivot(unsigned char Arr[], const  long int first);
unsigned char GetPivotMedianOfThree(unsigned char Arr[], const long first, const long last);

unsigned long int Partition(unsigned char* Arr, const unsigned long int first, const unsigned long int last, const unsigned char pivot)
{
    unsigned long int i = first;
    unsigned long int j = last;

    while(i <= j)
    {
        while(Arr[i] < pivot)
        {
            ++i;
        }
        while(Arr[j] > pivot)
        {
            --j;
        }
        if(i <= j)
        {
            swap(Arr[i],Arr[j]);
            if (i<last) ++i;
            if (j>first)--j;
        }
    }
    return i;
}

 long int Partition(unsigned char Arr[], const unsigned long int first, const unsigned long int last)
{
     unsigned char pivot = GetPivot(Arr, first);
     unsigned long int i = first;
     unsigned long int j = last;

    while(i <= j)
    {
        while(Arr[i] < pivot)
        {
            ++i;
        }
        while(Arr[j] > pivot)
        {
            --j;
        }
        if(i <= j)
        {
            swap(Arr[i],Arr[j]);
            if (i<last) ++i;
            if (j>first)--j;
        }
    }
    return i;
}

 unsigned long int PartitionSedgwick(unsigned char* Arr, const unsigned long int first, const unsigned long int last)
 {
     unsigned char pivot = Arr[last];
     unsigned long int i = first-1;
     unsigned long int j = last;
     for(;;)
     {
         while(Arr[++i]<pivot);
         while(pivot < Arr[--j]) if(j==first) break;
         if(i >= j) break;
         swap(Arr[i],Arr[j]);
     }
     swap(Arr[i],Arr[last]);
     return i;
 }

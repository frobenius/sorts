#ifndef QUICKSORT_H
#define QUICKSORT_H

unsigned long int QuickSort(unsigned char Arr[], const unsigned long int first, const unsigned long int last);
void QuickSortSedgwick(unsigned char Arr[], const unsigned long int first, const unsigned long int last);
void QuickSort2Threads(unsigned char* Arr, const unsigned long int first, const unsigned long int last);
void QuickInsertSort(unsigned char Arr[], const unsigned long int first, const unsigned long int last);
void QuickInsertHybridSort(unsigned char Arr[], const unsigned long int first, const unsigned long int last);
void QuickLazySort(unsigned char Arr[], const unsigned long int first, const unsigned long int last);

unsigned long int QuickSortTest(unsigned char Arr[], const unsigned long int first, const unsigned long int last);

#endif // QUICKSORT_H

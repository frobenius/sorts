#include <climits>
#include <thread>

#include "StackWatcher.h"


unsigned long int Partition(unsigned char* Arr, const unsigned long int first, const unsigned long int last, const unsigned char pivot);
long int Partition(unsigned char Arr[], const unsigned long int first, const unsigned long int last);
unsigned long int PartitionSedgwick(unsigned char* Arr, const unsigned long int first, const unsigned long int last);

unsigned long int QuickSort(unsigned char Arr[], const unsigned long int first, const unsigned long int last)
{
    StackWatcher::addStackCall();
    static unsigned long int reqursionNum = 0;
    long int p;
    if(first < last)
    {
        p = Partition(Arr, first, last);
        QuickSort(Arr, first, p-1);
        QuickSort(Arr, p, last);
    }
    StackWatcher::removeStackCall();
    ++ reqursionNum;
    return reqursionNum;
}

void QuickSortSedgwick(unsigned char Arr[], const unsigned long int first, const unsigned long int last)
{
    if((last <= first)||(last == ULONG_MAX))return;

    unsigned long int i = PartitionSedgwick(Arr, first, last);
    QuickSortSedgwick(Arr, first, i-1);
    QuickSortSedgwick(Arr, i+1, last);
}


void QuickSort2Threads(unsigned char* Arr, const unsigned long int first, const unsigned long int last)
{
    unsigned long int p0;
    unsigned long int p1;
    unsigned long int p2;

    //qtime.start();
    p0 = Partition(Arr, first, last);
    p1 = Partition(Arr, first, p0-1);
    p2 = Partition(Arr, p0, last);

    //printf("Middle pivot at: %f\n",100.0f * float(p0)/float(last - first));
    //std::cout << "Kernels: " <<std::thread::hardware_concurrency() <<"\n";
    //std::cout << "middle pivot at " << 100.0f * float(p0)/float(last - first) << " %" <<"\n";
    //std::cout << "first pivot at " << 100.0f * float(p1)/float(p0 - first) << " %" <<"\n";
    //std::cout << "last pivot at " << 100.0f * float(p2-p0)/float(last - p0) << " %" <<"\n";

    //std::thread t1(QuickSort, Arr, first, p0-1);
    //std::thread t2(QuickSort, Arr, p0, last);


    std::thread t1(QuickSort, Arr, first, p1-1);
    std::thread t2(QuickSort, Arr, p1, p0-1);
    std::thread t3(QuickSort, Arr, p0, p2-1);
    std::thread t4(QuickSort, Arr, p2, last);


    t1.join();
    t2.join();
    t3.join();
    t4.join();
    //qtime.elapsed();
    //printf("qtime: %f\n",qtime.elapsed()/1000.0);
}

void InsertionSort3(unsigned char Arr[], const unsigned long int first, const unsigned long int last);
void QuickLazySort(unsigned char Arr[], const unsigned long int first, const unsigned long int last)
{
    if( ( (last - first) <= 10 ) || (last <= first) || (last == ULONG_MAX) )
    {
        return;
    }

    unsigned long int i;
    i = PartitionSedgwick(Arr, first, last);

    if( (i-first)<(last-i) )
    {
        QuickLazySort(Arr, first, i-1);
        QuickLazySort(Arr, i+1, last);
    }
    else
    {
        QuickLazySort(Arr, i+1, last);
        QuickLazySort(Arr, first, i-1);
    }
}

void QuickInsertHybridSort(unsigned char Arr[], const unsigned long int first, const unsigned long int last)
{
    QuickLazySort(Arr, first, last);
    InsertionSort3(Arr, first, last);
}

unsigned long int QuickSortTest(unsigned char Arr[], const unsigned long int first, const unsigned long int last)
{
    //QuickSort(Arr, first, last);
    //QuickSortSedgwick(Arr, first, last);
    //QuickSort2Threads(Arr, first, last);//Not Pass?
    QuickInsertHybridSort(Arr, first, last);
}

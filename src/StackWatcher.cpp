#include <StackWatcher.h>

size_t StackWatcher::count = 0;
void StackWatcher::addStackCall()
{
    StackWatcher::count += 1;
    printStackCount();
}

void StackWatcher::removeStackCall()
{
    StackWatcher::count -= 1;
}

void StackWatcher::printStackCount()
{
    for (size_t i = 0; i < count; ++i)
        std::cout << ((i%10) ? "." : "|");
    std::cout << "\n";
}

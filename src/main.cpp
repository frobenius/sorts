#include <stdio.h>
#include <ctime>
#include <time.h>
#include <climits>
#include <cstdlib>

#include "SortAlgorithmsPolynomial.h"
//#include "StackWatcher.h"
#include <QuickSort/QuickSort.h>
#include <MergeSort/MergeSort.h>

unsigned long int reqursionNum = 0;
int main()
{
    printf("BEGIN - Sort Algorithms\n");

    //const unsigned long int arr_size = 8;
    //const unsigned long int arr_size = 16;
    //const unsigned long int arr_size = 32;
    //const unsigned long int arr_size = 64;
    //const unsigned long int arr_size = 128;
    //const unsigned long int arr_size = 256;
    //const unsigned long int arr_size = 512;
    //const unsigned long int arr_size = 1024;//Kb
    //const unsigned long int arr_size = 1048576;//Mb
    //const unsigned long int arr_size = 1073741824;//Gb

    //const unsigned long int arr_size = 1048576;

    //const unsigned long int arr_size = 65535;
    const unsigned long int arr_size = 5;
    //const unsigned long int arr_size = 100;
    //unsigned char* ArrTemp = new unsigned char[arr_size];
    //unsigned char* TestArr = new unsigned char[arr_size];
    unsigned char TestArr[] = {7, 3, 9, 1, 9};//3 6 9 0 8
    //int TestArr[] = {3, 6, 9, 0, 8};//3 6 9 0 8
    //int TestArr[] = {88, 49, 75, 2, 93, 27, 97, 35, 26, 65};
    //int TestArr[] = {14, 4, 56, 95, 34, 94, 70, 3, 25, 95};
    //int TestArr[] = {10, 9, 8, 7, 6, 5, 4, 3, 2, 1};
    //FillArr(TestArr,arr_size);

    clock_t duration;

    printf("Unsort:\n");
    PrintArr(TestArr,arr_size);

    /// SORT /////////////////////
    duration = std::clock();
    //BubbleSort(TestArr, arr_size);
    //GnomeSort(TestArr, arr_size);
    //InsertionSort(TestArr, arr_size);
    //InsertionSort2(TestArr, arr_size);
    //InsertionSort3(TestArr, arr_size);
    //InsertionSort4(TestArr, 0, arr_size -1);
    //SelectionSort(TestArr, arr_size);
    //SelectionSortDouble(TestArr, arr_size);
    //CocktailShakerSort(TestArr, arr_size);
    //MergeSort(TestArr, ArrTemp, 0, arr_size-1);
    QuickSortSedgwick(TestArr, 0, arr_size-1);
    //QuickSort(TestArr, 0, arr_size-1);
    //quickSort_2threads(TestArr, 0, arr_size-1);
    //QuickInsertHybridSort(TestArr, 0, arr_size-1);
    //qsort (TestArr, arr_size, sizeof(unsigned char), compare);
    //printf("max bisection recursion = %i\n",max_bisection_recursion(TestArr, 0, arr_size));
    //printf("max = %i\n",max(TestArr, arr_size));
    duration = std::clock() - duration;
    //////////////////////////////

    printf("duration time: %li processor's clock ticks.\n",duration);
    printf("duration time: %f seconds.\n",(float(duration))/CLOCKS_PER_SEC);

    printf("Sort:\n");
    PrintArr(TestArr,arr_size);

    if(isArrSort(TestArr, arr_size)){printf("Array is sort.\n");}
    else{printf("Array is not sort.\n");}
    printf("Reqursion Num - %lu\n",reqursionNum);

    printf("\n");
    printf("END - Sort Algorithms\n");
    //delete[] TestArr;
    //delete[] ArrTemp;
    return 0;
}


#include "SortAlgorithmsPolynomial.h"

////////////////////////Sorting algorithms////////////////////////////////////////////

void BubbleSort(unsigned char Arr[], const unsigned long int size)
{
    for(unsigned long int i=0;i<size-1;++i)
    {
        for(unsigned long int j=0;j< size-1-i;++j)
        {
            if(Arr[j]>Arr[j+1]) swap(Arr[j],Arr[j+1]);
        }
    }
}

void GnomeSort(unsigned char Arr[], const unsigned long int size)
{
    unsigned long int i = 1;
    while(i < size)
    {
        if( (i == 0) || (Arr[i - 1] <= Arr[i]) )
        {
            ++i;
        }
        else
        {
            swap(Arr[i - 1],Arr[i]);
            --i;
        }
    }
}

void InsertionSort(unsigned char Arr[], const unsigned long int size)
{
    unsigned long int j = 0;

    for(unsigned long int i=0;i<size;++i)
    {
        j = i;
            while( (j>0)&&(Arr[j]<Arr[j-1]) )
            {
                swap(Arr[j],Arr[j-1]);
                --j;
            }
    }
}

void InsertionSort2(unsigned char Arr[], const unsigned long int size)
{
    unsigned long int k;

    unsigned long int min_index = index_min(Arr,size);
    swap_and_shift_rigft(Arr, 0, min_index);

    for(unsigned long int i=1;i<size;++i)
    {
        if(Arr[i-1] > Arr[i])
        {
            k = i - 1;
            while( (Arr[k] > Arr[i]))
            {
                --k;
            }
            swap_and_shift_rigft(Arr, k+1, i);
        }
    }
}

void InsertionSort3(unsigned char Arr[], const unsigned long int first, const unsigned long int last)
{
    unsigned long int j = 0;

    for(unsigned long int i=first+1; i < last+1; ++i)
    {
        j = i;
            while( (j>first)&&(Arr[j]<Arr[j-1]) )
            {
                swap(Arr[j],Arr[j-1]);
                --j;
            }
    }
}

void SelectionSort(unsigned char Arr[], const unsigned long int size)
{
    unsigned char min;
    unsigned long int min_index = 0;
    for(unsigned long int i = 0; i < size ; ++i)
    {
        min = Arr[i];
        min_index = i;
        for(unsigned long int j = i; j < size; ++j)
        {
            if(min > Arr[j])
            {
                min = Arr[j];
                min_index = j;
            }
        }
        swap(Arr[min_index],Arr[i]);
    }
}

void SelectionSortDouble(unsigned char Arr[], const unsigned long int size)
{
    unsigned char max, min;
    unsigned long int max_index, min_index;
    unsigned long int left;
    unsigned long int right = size-1;

    min_index = index_min(Arr,size);
    swap(Arr[min_index],Arr[0]);

    for(left = 1; left < right;)
    {
        min = Arr[left];
        min_index = left;
        for(unsigned long int j = left; j <= right; ++j)
        {
            if(min > Arr[j])
            {
                min = Arr[j];
                min_index = j;
            }
        }
        swap(Arr[min_index],Arr[left]);
        ++left;

        max = Arr[right];
        max_index = right;
        for(unsigned long int j = right; j >= left; --j)
        {
            if(max < Arr[j])
            {
                max = Arr[j];
                max_index = j;
            }
        }
        swap(Arr[max_index],Arr[right]);
        --right;
    }
}

void CocktailShakerSort(unsigned char Arr[], const unsigned long int size)
{
    unsigned long int min_index;
    unsigned long int left = 1;
    unsigned long int right = size-1;

    min_index = index_min(Arr,size);
    swap(Arr[min_index],Arr[0]);
    for(left = 1; left < right;)
    {
        for(unsigned long int j = left; j < right; ++j)
        {
            if(Arr[j] > Arr[j+1])
            {
                swap(Arr[j],Arr[j+1]);
            }
        }
        --right;

        for(unsigned long int j = right-1; j >= left; --j)
        {
            if(Arr[j] > Arr[j+1])
            {
                swap(Arr[j],Arr[j+1]);
            }
        }
        ++left;
    }
}

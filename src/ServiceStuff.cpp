#include "ServiceStuff.h"

////////////////////////Auxiliary functions///////////////////////////////////////////
void swap(unsigned char &a, unsigned char &b)
{
    unsigned char tmp = a;
    a = b;
    b = tmp;
}

void swap_and_shift_rigft(unsigned char Arr[], const unsigned long int i, const unsigned long int j)
{
        unsigned char tmp = Arr[j];
        for(unsigned long int k = j; k>i; --k)
        {
            Arr[k] = Arr[k-1];
        }
        Arr[i] =  tmp;
}

int max(unsigned char Arr[], const unsigned long int size)
{
    unsigned char max = Arr[0];
    for(unsigned long int i = 1; i < size; ++i)
    {
        if(max<Arr[i]){max=Arr[i];}
    }
    return max;
}

int min(unsigned char Arr[], const unsigned long int size)
{
    unsigned char min = Arr[0];
    for(unsigned long int i = 1; i < size; ++i)
    {
        if(min>Arr[i]){min=Arr[i];}
    }
    return min;
}

int max_bisection_recursion(int Arr[], const unsigned int begin, const unsigned int size)
{
    static unsigned int recursionNum = 1;
    unsigned char res, res_left, res_right;
    unsigned long int size_left, size_right;
    unsigned long int begin_left, begin_right;

    printf("recursionNum = %u\n",recursionNum);
    ++recursionNum;

    if(size > 1)
    {
        size_right  = size/2;
        size_left   = size - size_right;
        begin_left  = begin;
        begin_right = begin_left + size_left;

        res_left  = max_bisection_recursion(Arr, begin_left, size_left);
        res_right = max_bisection_recursion(Arr, begin_right, size_right);
        if(res_left > res_right){res = res_left;}
        else{res = res_right;}
    }
    else{ res = Arr[begin];}

    return res;
}

unsigned int index_max(unsigned char Arr[], const unsigned long int size)
{
    unsigned long int index = 0;
    unsigned char max = Arr[0];
    for(unsigned long int i=1;i<size;++i)
    {
        if(max<Arr[i])
        {
            max = Arr[i];
            index = i;
        }
    }
    return index;
}

unsigned int index_min(unsigned char Arr[], const unsigned long int size)
{
    unsigned long int index = 0;
    unsigned char min = Arr[0];
    for(unsigned int i=1;i<size;++i)
    {
        if(min>Arr[i])
        {
            min = Arr[i];
            index = i;
        }
    }
    return index;
}

bool isArrSort(unsigned char Arr[], const unsigned long int size)
{
    bool isSort = true;
    for(unsigned long int i=0;i<size-1;++i)
    {
        if(Arr[i]>Arr[i+1]){
            isSort = false;
            break;}
    }
    return isSort;
}

void FillArr(unsigned char Arr[], const unsigned long int size)
{
    std::srand(time(NULL));
    for(unsigned long int i= 0;i<size;++i)
    {
        Arr[i] = std::rand()%100;
    }
}

void PrintArr(unsigned char Arr[], const unsigned long int size)
{
    for(unsigned long int i=0;i<size;++i)
    {
        printf("%u ",Arr[i]);
    }
    printf("\n");
}

int compare (const void * a, const void * b)
{
    return ( *(unsigned char*)a - *(unsigned char*)b );
}

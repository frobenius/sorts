#-------------------------------------------------
#
# Project created by QtCreator 2017-09-22T00:09:38
#
#-------------------------------------------------

QT       += core

QT       -= gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SortAlgorithms
CONFIG   += console
CONFIG   -= app_bundle
CONFIG   += warn_on
#CONFIG   += warn_off


TEMPLATE = app


SOURCES += $$PWD/src/main.cpp \
    $$PWD/src/SortAlgorithmsPolynomial.cpp \
    $$PWD/src/ServiceStuff.cpp \
    $$PWD/src/StackWatcher.cpp \
    QuickSort/QuickSort.cpp \
    MergeSort/MergeSort.cpp \
    QuickSort/GetPivot.cpp \
    QuickSort/Partition.cpp \
    MergeSort/Merge.cpp

HEADERS += \
    $$PWD/inc/SortAlgorithmsPolynomial.h \
    $$PWD/inc/ServiceStuff.h \
    $$PWD/inc/StackWatcher.h \
    QuickSort/QuickSort.h \
    MergeSort/MergeSort.h

INCLUDEPATH += $$PWD/src \
               $$PWD/inc

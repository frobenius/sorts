#ifndef SORTALGORITHMSPOLYNOMIAL_H
#define SORTALGORITHMSPOLYNOMIAL_H

#include <stdio.h>
#include <ctime>
#include <time.h>
#include <climits>
#include <cstdlib>

#include "ServiceStuff.h"

void BubbleSort(unsigned char Arr[], const unsigned long int size);
void GnomeSort(unsigned char Arr[], const unsigned long int size);
void InsertionSort(unsigned char Arr[], const unsigned long int size);
void InsertionSort2(unsigned char Arr[], const unsigned long int size);
void InsertionSort3(unsigned char Arr[], const unsigned long int first, const unsigned long int last);
void SelectionSort(unsigned char Arr[], const unsigned long int size);
void SelectionSortDouble(unsigned char Arr[], const unsigned long int size);
void CocktailShakerSort(unsigned char Arr[], const unsigned long int size);

#endif // SORTALGORITHMSPOLYNOMIAL_H

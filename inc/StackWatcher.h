#ifndef STACKWATCHER_H
#define STACKWATCHER_H

#include <iostream>

class StackWatcher
{
public:
    static size_t count;
    static void addStackCall();
    static void removeStackCall();
    static void printStackCount();
private:
    StackWatcher();
    ~StackWatcher();
    StackWatcher(StackWatcher&);
    StackWatcher& operator=(StackWatcher&);
};

#endif // STACKWATCHER_H

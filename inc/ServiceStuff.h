#ifndef SERVICESTUFF_H
#define SERVICESTUFF_H

#include <stdio.h>
#include <ctime>
#include <time.h>
#include <climits>
#include <cstdlib>

void swap(unsigned char &a, unsigned char &b);
void swap_and_shift_rigft(unsigned char Arr[], const unsigned long int i, const unsigned long int j);
int max(unsigned char Arr[], const unsigned long int size);
int min(unsigned char Arr[], const unsigned long int size);
int max_bisection_recursion(unsigned char Arr[], const unsigned long int begin, const unsigned long int size);
unsigned int index_max(unsigned char Arr[], const unsigned long int size);
unsigned int index_min(unsigned char Arr[], const unsigned long int size);
bool isArrSort(unsigned char Arr[], const unsigned long int size);
void FillArr(unsigned char Arr[],const unsigned long int size);
void PrintArr(unsigned char Arr[],const unsigned long int size);
int compare (const void * a, const void * b);


#endif // SERVICESTUFF_H

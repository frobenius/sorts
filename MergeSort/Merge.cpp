
void merge(unsigned char Arr[], unsigned char ArrBuff[], const unsigned long int first,  const unsigned long int last)
{
    unsigned long int midle = (last+first)/2;
    unsigned long int Li = first;
    unsigned long int Ri = midle+1;
    unsigned long int i = first;

    while((Li<=midle)&&(Ri<=last))
    {
        if(Arr[Li]<Arr[Ri])
        {
            ArrBuff[i] = Arr[Li];
            ++Li;
            ++i;
        }
        else
        {
            ArrBuff[i] = Arr[Ri];;
            ++Ri;
            ++i;
        }
    }
    if(Li>midle)
    {
        for(unsigned long int j=i;j<=last;++j)
        {
            ArrBuff[j] = Arr[Ri];
            ++Ri;
        }
        for(unsigned long int j=first;j<=last;++j)
        {
            Arr[j]=ArrBuff[j];
        }
        return;
    }
    if(Ri>last)
    {
        for(unsigned long int j=i;j<=last;++j)
        {
            ArrBuff[j] = Arr[Li];
            ++Li;

        }
        for(unsigned long int j=first;j<=last;++j)
        {
            Arr[j]=ArrBuff[j];
        }
        return;
    }
}


void merge(unsigned char Arr[], unsigned char ArrBuff[], const unsigned long int first,  const unsigned long int last);

void MergeSort(unsigned char Arr[], unsigned char ArrBuff[], const unsigned long int first,  const unsigned long int last)
{
    const unsigned long int Lfirst = first;
    const unsigned long int Llast  = (last + first)/2;
    const unsigned long int Rfirst = (last + first)/2 +1;
    const unsigned long int Rlast  = last;

    if(first < last)
    {
        MergeSort(Arr, ArrBuff, Lfirst, Llast);
        MergeSort(Arr, ArrBuff, Rfirst, Rlast);
        merge(Arr, ArrBuff, first, last);
    }
}

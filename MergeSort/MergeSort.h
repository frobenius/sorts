#ifndef MERGESORT
#define MERGESORT

typedef unsigned char uchar;
typedef unsigned long int ulong;

void MergeSort(uchar Arr[], uchar ArrBuff[], const ulong first, const ulong last);

#endif // MERGESORT


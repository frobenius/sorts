TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += qsort_test.cpp \

SOURCES += \
    $$PWD/../src/ServiceStuff.cpp \
    $$PWD/../src/SortAlgorithmsPolynomial.cpp \
    $$PWD/../src/StackWatcher.cpp \
    $$PWD/../QuickSort/GetPivot.cpp \
    $$PWD/../QuickSort/Partition.cpp \
    $$PWD/../QuickSort/QuickSort.cpp

HEADERS += \
    $$PWD/../inc/ServiceStuff.h \
    $$PWD/../inc/SortAlgorithmsPolynomial.h \
    $PWD/../inc/StackWatcher.h \
    $$PWD/../QuickSort/QuickSort.h

INCLUDEPATH += $$PWD/../third_party/inc\
               $$PWD/../src \
               $$PWD/../inc

LIBS += -lgtest -lgtest_main -pthread

